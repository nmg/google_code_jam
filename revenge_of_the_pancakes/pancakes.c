#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*User can redefine the input string size as needed*/
#define LEN 101

char *flip(char *str, int end);

int main (void) {
	int T, i, j, k, l, flips, len;
	T= i= j= k= l= flips= len= 0;
	char *str_arr= NULL;
	char *cmp_str= NULL;
	int *num= NULL;
	/*getting number of text cases T*/
	scanf("%d", &T);
	for (i= 0; i < T; i++) {
		flips= 0;
		str_arr= calloc(LEN, sizeof(char));
		cmp_str= calloc(LEN, sizeof(char));
		scanf("%s", str_arr); /*scanning in the pancake string*/
		len= strlen(str_arr); 
		/*constructing the ideal final string*/
		for (j= 0; j < len; j++)
			cmp_str[j]= '+';
		/*keeps flipping until the inputted string is the ideal string*/
		while (strcmp(str_arr, cmp_str) != 0) {
			/*first find the first k chars that are '+' and flip the all to '-'*/
			k= 0;
			while (str_arr[k] == '+' && k < len)
				k++;
			if (k) {
				flips++; /*counts as 1 flip*/
				str_arr= flip(str_arr, k-1);
			}
			/*then finds the last char that is '-' and flips the stack 
			up to and including that char*/
			l= 0;
			while (str_arr[len - l - 1] == '+' && l < len)
				l++;
		
			if (l < len) {
				flips++; /*counts as 1 flip*/
				str_arr= flip(str_arr, len - l - 1);
			}
		}
		printf("Case #%d: %d\n", i + 1, flips);
		
		free(str_arr);
		free(cmp_str);
	}
	
	return 0;
}

/*flips a substring up to and including index end*/
char *flip(char *str, int end) {
	int i = 0;
	char *new = calloc(LEN, sizeof(char));
	strcpy(new, str);
	while(i <= end) {
		str[i]= new[end - i];
		str[i] = str[i] == '+' ? '-':'+';
		i++;
	}
	free(new);
	return str;
}

