#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*LEN corresponds to the strlen of the inputted number plus 1 for the 
null character*/
#define LEN 8


int main (void) {
	int T= 0, i= 0, j= 0, k= 1, input= 0, result= -1, 
	new_num= 0, check_sum= 0, iter_num= 0;
	char *str_arr= NULL;
	int *num= NULL;
	/*scan in the number of cases*/
	scanf("%d", &T);
	/*iterate through all cases*/
	for (i= 0; i < T; i++) {
		str_arr= calloc(LEN, sizeof(char));
		/*creating an int array that holds all the found digits*/
		num= calloc(10, sizeof(int)); 
		scanf("%s", str_arr);
		/*reading in the given number into int type*/
		sscanf(str_arr, "%d", &input);
		k= 1;
		check_sum= 0;
		/*keep iterating until the check sum is 10 (all digits 0-9 found)*/
		while (check_sum != 10 && input != 0) {
			/*the input is modified by which multiplier k it is multiplied by.
			new_num is an int that is modified to determine the individual 
			digits of the number input*/
			new_num= input*k++;
			/*iterates through new num until all the digits are recovered*/
			while (new_num != 0) {
				result= new_num % 10; /*one particular digit*/
				num[result] = 1; /*result corresponds to indices of the num arr*/
				new_num /= 10; /*new num is now the next most sig digit*/
			}
			/*going through the num array to see if all digits 0-9 are recovered*/
			check_sum= 0;
			for (j= 0; j < 10; j++) 
				check_sum += num[j];
		}
		/*prints case number*/
		printf("Case #%d: ", i + 1);
		/*If 0 was the input, "INSOMNIA" is printed because not all the numbers
		can be recovered in the 0 case*/
		if (input == 0)
			printf("INSOMNIA\n");
		/*otherwise the number that completed the check sum (getting all the
		digits) is printed*/
		else
			printf ("%d\n",input);
		/*freeing allocated blocks. In this case, the heap didn't need to be 
		used. But can never be too careful!*/
		free(str_arr);
		free(num);
		/*everything initialized to zero so the next case isn't disrupted*/
		j= read_in= input = 0;
	}
	
	return 0;
}

