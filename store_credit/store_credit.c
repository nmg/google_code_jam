#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define I_MAX 2000

/**Nevo Magnezi - Computer Engineering UMD 2018
Email: magnezin@gmail.com

The problem that this program addresses was created by Google and 
this is but one student solution. This solution complies with 
all of Google's Terms and Conditions, please do the same 
when using this code. This code  assumes 
the reader is familiar with the prompt of the problem. 
*/

int *calculate(int c, int i, int *p);

int main (void) {
	int n = 0, c = 0, i = 0, *p = calloc(I_MAX, sizeof(int)), count = 0, count2=0,
		*ans = NULL, ind1 = -1, ind2 = -2;
	FILE *f = fopen("A-large-practice.in", "r");
	FILE *f2 = fopen("output.out", "w");
	if ( f == NULL || f2 == NULL)
		exit(-1);
	fscanf(f, "%d", &n);
	for ( count = 0; count < n; count++) {
		fscanf(f, "%d", &c);
		fscanf(f,"%d", &i);
		for (count2 = 0; count2 < i; count2++) 
			fscanf(f,"%d", &p[count2]);
		ans = calculate(c, i, p);
		ind1 = ans[0];
		ind2 = ans[1];
		free(ans);
		fprintf(f2, "Case #%d: %d %d\n", count+1, ind1+1, ind2+1); 
	}
	free(p);
	fclose(f);
	fclose(f2);
	return 0;
}

int *calculate(int c, int i, int *p) {
	int k = 0, j = 0, *arr = calloc(2, sizeof(int));
	arr[0] = arr[1] = -1;
	for (j = 0; j < i; j++) 
		for (k = 0; k < i; k ++) {
			if (j != k)
				if (p[j] + p[k] == c) {
					arr[0] = j < k ? j : k;
					arr[1] = j < k ? k : j;
					return arr;
				}
		}
	exit(-1);
	return NULL;	
}
