/**
	Nevo Magnezi - University of Maryland - Computer Engineering - 2018
	Google Code Jam Challenge
	https://code.google.com/codejam/contest/4304486/dashboard
	
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*SZ corresponds to the maximum char array size of any individual input*/
#define SZ 1001


int main (void) {
	int T= 0, len= 0, i= 0, j= 0, k= 0;
	char *str= NULL;
	char *new_str= NULL;
	
	/*scan in the number of cases*/
	scanf("%d", &T);
	
	/*iterate through all cases*/
	for (i= 0; i < T; i++) {
		/*declaring str and new_str in the heap to be size SZ*/
		str= calloc(SZ, sizeof(char));
		new_str= calloc(SZ, sizeof(char));
		
		/*scans in the string for case i*/
		scanf("%s", str);
		len= strlen(str);
		/*first element of new string always gets first elem of input string*/
		new_str[0] = str[0];
		
		/*goes through the rest of the elements in the old string to put in the
		new string*/
		for (j= 1; j < len; j++) {
		
			/*if the letter to be added comes earlier in the alphabet than the 
			first letter, it is added at the end of the current word*/
			if (str[j] < new_str[0]) 
				new_str[j]= str[j];
			
			/*if the letter to be added is equal or comes later in the alphabet,
			it is added to the beginning of the current word*/
			else {
			
				/*all the letters must be shifted by one first*/
				for (k= j; k > 0; k--)
					new_str[k]= new_str[k - 1];
				/*new letter added to beginning of word*/
				new_str[0] = str[j];
			} /*end of else*/
		} /*end of for loop which iterates with j*/
		
		/*prints case number*/
		printf("Case #%d: %s\n", i + 1, new_str);
		
		/*freeing allocated blocks. In this case, the heap doesn't technically
		need to be used. But this makes it easier to redeclare the two utilized
		strings*/
		free(str);
		free(new_str);
	}
	
	return 0;
}

